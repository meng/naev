Source: naev
Section: games
Priority: optional
Maintainer: Federico Ceratto <federico@debian.org>
Build-Depends: debhelper-compat (= 13),
 autoconf-archive,
 automake,
 autopoint,
 binutils-dev,
 cmake,
 gettext,
 intltool,
 libfreetype6-dev,
 libgl1-mesa-dev,
 libiberty-dev,
 liblua5.1-dev,
 libopenal-dev,
 libpng-dev,
 libsdl2-dev,
 libsdl2-image-dev,
 libsdl2-mixer-dev,
 libsuitesparse-dev,
 libunibreak-dev,
 libunibreak1,
 libvorbis-dev,
 libxml2-dev,
 meson,
 ninja-build,
 python3-yaml,
 xvfb
Standards-Version: 4.5.1
Homepage: https://naev.org
Vcs-Browser: https://salsa.debian.org/debian/naev
Vcs-Git: https://salsa.debian.org/debian/naev.git
Rules-Requires-Root: no

Package: naev
Architecture: alpha amd64 arm64 armel armhf hppa i386 ia64 loong64 m68k mips mips64el mipsel powerpc powerpcspe ppc64 ppc64el riscv64 s390x sh4 sparc64 x32
Depends: ${shlibs:Depends}, ${misc:Depends},
 naev-data (= ${source:Version})
Description: 2D action/rpg space game
 2D space trading and combat game, in a similar vein to Escape Velocity. Naev
 is played from a top-down perspective, featuring fast-paced combat, many
 ships, a large variety of equipment and a large galaxy to explore.
 The game is open-ended, letting you proceed at your own pace.

Package: naev-data
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: naev
Description: 2D action/rpg space game - game data
 2D space trading and combat game, in a similar vein to Escape Velocity. Naev
 is played from a top-down perspective, featuring fast-paced combat, many
 ships, a large variety of equipment and a large galaxy to explore.
 The game is open-ended, letting you proceed at your own pace.
 This packages contains architecture-independent game data.
